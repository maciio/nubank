# Simple Account balance application

### Simple statement
This is a simple statement application build with gradle 5, groovy 2.5 and H2 DB

* [Official Gradle documentation](https://docs.gradle.org)
* [Official Groovy documentation](http://groovy-lang.org/documentation.html)
* [Official H2 DB Engine documentation](https://www.h2database.com/html/main.html)

### Gradle
To local use install the same gradle version set in the `gradle.properties`, or use wrapper (`./gradlew`) instead.

To build the project 'statement':

    gradlew clean build

To run the project 'statement':

    gradlew bootrun
*The project will run locally at `http://localhost:9090`

To get the coverage project 'statement' report:

    gradlew build jacocoTestReport
*The report will be placed under the path: `statement/build/reports/jacoco/test/html/index.html`

### Functional Testing

To add a new operation:
 - [POST] `/account/operation` with operations json objects like:
  ```
  {
    "accountNo": "UniqueNumber",
    "description": "description",
    "operationType": "DEPOSIT *",
    "amount": "100.50 **",
    "appliedAt" : "05-01-2019@07:53:34 ***"    
  }
  ```
  *Valid operationTypes: [DEPOSIT, WITHDRAWAL, PURCHASE] <br />
  **Amount should be have 2 decimals <br />
  ***The date should follow the `MM-dd-yyyy@HH:mm:ss` pattern (this indicates exactly when the tx happened) <br /> 
 
To get the balance:
- [GET] `/account/${accountNumber}/balance`; e.g. -> `/account/667/balance` 

To get the statement:
- [GET] `/account/${accountNumber}/statement` alogn with the *required* request params:
  - startDate *
  - endDate *

To find the periods of debts between dates:
- [GET] `/account/${accountNumber}/debt-periods` alogn with the *required* request params:
  - startDate *
  - endDate *

* The format for this dates is `yyyy-MM-dd`


### NOTES:

It follows the standar that every request response result in OK, in order to prevent security risks.

TODOS: 
- need to extract common constants for common responses codes
 

