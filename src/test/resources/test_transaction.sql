delete from operation;
delete from account;
insert into account (account_no, balance, created_at, update_at, id) values ('111', 100.00, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP ,'shalalala321');
insert into operation (account_id, amount, applied_at, created_at, description, operation_type, id) values ('shalalala321', 125, '2019-01-10 18:47:52.69', CURRENT_TIMESTAMP, 'Deposit x', 0, 'ishalala0');
insert into operation (account_id, amount, applied_at, created_at, description, operation_type, id) values ('shalalala321', 25, '2019-01-11 17:57:52.69', CURRENT_TIMESTAMP, 'withdraw y', 1, 'ishalala1');
insert into operation (account_id, amount, applied_at, created_at, description, operation_type, id) values ('shalalala321', 50, '2019-01-11 10:47:52.69', CURRENT_TIMESTAMP, 'purchase z', 2, 'ishalala2');
insert into operation (account_id, amount, applied_at, created_at, description, operation_type, id) values ('shalalala321', 51, '2019-01-11 10:47:52.69', CURRENT_TIMESTAMP, 'purchase a', 2, 'ishalala3');
insert into operation (account_id, amount, applied_at, created_at, description, operation_type, id) values ('shalalala321', 16, '2019-01-11 10:47:52.69', CURRENT_TIMESTAMP, 'deposit b', 0, 'ishalal4');
