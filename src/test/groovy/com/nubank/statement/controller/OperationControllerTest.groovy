package com.nubank.statement.controller

import com.nubank.statement.entity.Account
import com.nubank.statement.entity.dto.DebtDTO
import com.nubank.statement.entity.dto.OperationDTO
import com.nubank.statement.entity.dto.StatementDTO
import com.nubank.statement.service.AccountService
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc

import static org.hamcrest.Matchers.containsString
import static org.mockito.ArgumentMatchers.any
import static org.mockito.Mockito.when
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Created by Mario A. Pineda on 2019-06-09.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(OperationController.class)
class OperationControllerTest {

  @Autowired
  private MockMvc mockMvc

  @MockBean
  private AccountService accountService


  @Test
  void testAddOperation() {
    Account account = new Account(id: "xxxId", accountNo: "777")
    when(accountService.addOperation(any())).thenReturn(account)
    String cont = "{\"accountNo\":\"777\",\"description\":\"Transfer wise\",\"operationType\":\"DEPOSIT\",\"amount\":\"99.00\",\"appliedAt\":\"2019-05-01@07:53:35\"}"
    this.mockMvc.perform(post("/account/operation")
        .content(cont)
        .accept("application/json")
        .contentType("application/json"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("OAS")))
  }

  @Test
  void testAddOperationFailed() {
    Account account = new Account(id: "xxxId", accountNo: "777")
    when(accountService.addOperation(any())).thenReturn(account)
    String cont = "{\"accountNo\":\"777\",\"description\":\"Transfer wise\",\"operationType\":\"DEPOSIT\",\"amount\":\"99.00\",\"appliedAt\":\"2019-05-01@07:53:35\"}"
    this.mockMvc.perform(post("/account/operation")
        .accept("application/json")
        .contentType("application/json"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("SOO")))
  }

  @Test
  void testgetAccountBalance() {
    when(accountService.getBalance(any())).thenReturn(100.0G)
    this.mockMvc.perform(get("/account/123/balance"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("100.00")))
  }

  @Test
  void testgetAccountBalanceFailed() {
    this.mockMvc.perform(get("/account/a31/balance"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("IDA")))
  }

  @Test
  void testgetAccountStatement() {
    OperationDTO opDto = new OperationDTO(description: "Deposit - Transfer wise", amount: 99.00G)
    StatementDTO dto0 = new StatementDTO(date: "25/04/2019", operations: [opDto], balance: 99.00G)
    List<StatementDTO> dtos = []
    dtos << dto0
    when(accountService.getStatement(any(), any(), any())).thenReturn(dtos)
    this.mockMvc.perform(get("/account/123/statement?startDate=2019-04-25&endDate=2019-05-28"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("Deposit - Transfer wise")))
  }

  @Test
  void testgetAccountDebts() {
    DebtDTO dto0 = new DebtDTO(principal: 1000.0G, start: "26/05/2019", end: "27/05/2019")
    List<StatementDTO> dtos = []
    dtos << dto0
    when(accountService.getPeriodsOfDebts(any(), any(), any())).thenReturn(dtos)
    this.mockMvc.perform(get("/account/123/debt-periods?startDate=2019-04-25&endDate=2019-05-28"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("27/05/2019")))
  }


}
