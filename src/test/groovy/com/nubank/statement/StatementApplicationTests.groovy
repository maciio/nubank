package com.nubank.statement

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@SpringBootTest
class StatementApplicationTests {

  @Test
  void contextLoads() {
  }

  @Test
  void main() {
    StatementApplication.main([] as String[])
  }
}
