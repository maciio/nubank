package com.nubank.statement.service

import com.nubank.statement.entity.Account
import com.nubank.statement.entity.Operation
import com.nubank.statement.entity.dto.StatementDTO
import com.nubank.statement.repository.AccountRepository
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.test.context.junit4.SpringRunner

import java.time.LocalDate

import static org.hamcrest.CoreMatchers.equalTo
import static org.junit.Assert.assertThat
import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.anyString
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.when

/**
 * Created by Mario A. Pineda on 2019-06-07.
 */
@RunWith(SpringRunner.class)
public class AccountServiceTest {

  @Mock
  AccountRepository accountRepository

  @Mock
  OperationService operationService

  @InjectMocks
  AccountService accountService

  @Test
  void testAddOperation() {
    Account account = new Account(id: "shalalaID", accountNo: "777", balance: 50.0G)
    when(operationService.saveOperation(any(Operation.class))).thenReturn(account)

    Account acc = accountService.addOperation(new Operation())
    assertThat(acc.balance, equalTo(50.0G))
  }

  @Test
  void testGetBalance() {
    List accounts = []
    Account account = new Account(id: "shalalaID", accountNo: "777", balance: 50.0G)
    accounts << account
    when(accountRepository.findByAccountNo(anyString())).thenReturn(accounts)
    def result = accountService.getBalance("777")
    assertThat(result, equalTo(50.0G))

    when(accountRepository.findByAccountNo(anyString())).thenReturn([])
    result = accountService.getBalance("777")
    assertThat(result, equalTo(0.00G))
  }

  @Test
  void testGetStatement() {
    List accounts = []
    Account account = new Account(id: "shalalaID", accountNo: "777", balance: 50.0G)
    accounts << account

    List<StatementDTO> dtos= []
    StatementDTO dto = new StatementDTO(date: "06/06/2019", balance: 10.0G,)
    dtos << dto
    when(accountRepository.findByAccountNo(anyString())).thenReturn([])
    def result = accountService.getStatement("777", LocalDate.now(), LocalDate.now())
    verify(operationService, Mockito.times(0)).findOperationsbyAccountBetweenDates(any(),any(),any())
    assertThat(result, equalTo([]))

    when(accountRepository.findByAccountNo(anyString())).thenReturn(accounts)
    when(operationService.findOperationsbyAccountBetweenDates(any(), any(), any())).thenReturn(dtos)
    result = accountService.getStatement("777", LocalDate.now(), LocalDate.now())
    assertThat(result.size(), equalTo(1))
    assertThat(result.first().balance, equalTo("10.00"))

  }

  @Test
  void testGetPeriodsOfDebts() {
    List accounts = []
    Account account = new Account(id: "shalalaID", accountNo: "777", balance: 50.0G)
    accounts << account

    List<StatementDTO> dtos= []
    StatementDTO dto = new StatementDTO(date: "06/06/2019", balance: 10.0G,)
    dtos << dto
    when(accountRepository.findByAccountNo(anyString())).thenReturn([])
    def result = accountService.getPeriodsOfDebts("777", LocalDate.now(), LocalDate.now())
    verify(operationService, Mockito.times(0)).findDebts(any(),any(),any())
    assertThat(result, equalTo([]))

    when(accountRepository.findByAccountNo(anyString())).thenReturn(accounts)
    when(operationService.findDebts(any(), any(), any())).thenReturn(dtos)
    result = accountService.getPeriodsOfDebts("777", LocalDate.now(), LocalDate.now())
    assertThat(result.size(), equalTo(1))
    assertThat(result.first().balance, equalTo("10.00"))

  }
}
