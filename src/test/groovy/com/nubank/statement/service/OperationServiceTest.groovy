package com.nubank.statement.service

import com.nubank.statement.entity.Account
import com.nubank.statement.entity.Operation
import com.nubank.statement.repository.AccountRepository
import com.nubank.statement.repository.OperationRepository
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.test.context.junit4.SpringRunner

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate

import static com.nubank.statement.entity.OperationType.DEPOSIT
import static com.nubank.statement.entity.OperationType.WITHDRAWAL
import static org.hamcrest.Matchers.equalTo
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertThat
import static org.mockito.Matchers.any
import static org.mockito.Matchers.anyString
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.when

/**
 * Created by Mario A. Pineda on 2019-06-06.
 */
@RunWith(SpringRunner.class)
class OperationServiceTest {

  @Mock
  AccountRepository accountRepository

  @Mock
  OperationRepository operationRepository

  @InjectMocks
  OperationService operationService

  private static DateFormat df = new SimpleDateFormat('yyyy/MM/dd HH:mm:ss')

  @Before
  void setUp() {
  }


  @Test
  void testSaveNewOperation() {
    Account account = new Account(id: "xxxx", accountNo: "888")
    Operation operation = new Operation(
        amount: 123.56G,
        description: 'Simple Payment',
        operationType: DEPOSIT,
        accountNo: "888",
        appliedAt: new Date())

    when(accountRepository.findByAccountNo(anyString())).thenReturn(null)
    when(accountRepository.save(any())).thenReturn(account)
    when(operationRepository.save(any())).thenReturn(operation)

    operationService.saveOperation(operation)
    assertThat(operation.account, equalTo(account))
    verify(accountRepository, Mockito.times(1)).save(any())
  }


  @Test
  void testAddNewOperationAndUpdate() {
    List accounts = []
    Operation operation = new Operation(
        amount: 25.0G,
        description: 'Simple Payment',
        operationType: WITHDRAWAL,
        accountNo: "777",
        appliedAt: new Date())

    Account account = new Account(id: "shalalaID", accountNo: "777", balance: 50.0G, operations: [operation])
    accounts << account

    when(accountRepository.findByAccountNo(anyString())).thenReturn(accounts)
    when(accountRepository.save(any())).thenReturn(account)
    when(operationRepository.save(any())).thenReturn(operation)

    operationService.saveOperation(operation)
    assertThat(operation.account, equalTo(account))
    verify(accountRepository, Mockito.times(1)).save(any())
    assertThat(operation.account.balance, equalTo(25.0G))
  }

  @Test
  void findOperationsbyAccountBetweenDates() {
    Account account = new Account(id: "shalalaID", accountNo: "777")
    def initialBalance = 150.0G
    Operation operation1 = new Operation(
        amount: 100.0G,
        description: 'Transfer',
        operationType: DEPOSIT,
        account: account,
        appliedAt: df.parse('2019/06/09 01:12:55'))

    Operation operation2 = new Operation(
        amount: 25.0G,
        description: 'Simple Payment',
        operationType: WITHDRAWAL,
        account: account,
        appliedAt: df.parse('2019/06/09 06:12:22'))

    Operation operation3 = new Operation(
        amount: 25.0G,
        description: 'Simple Payment',
        operationType: WITHDRAWAL,
        account: account,
        appliedAt: df.parse('2019/06/07 06:12:22'))

    Operation operation4 = new Operation(
        amount: 25.0G,
        description: 'Simple Payment',
        operationType: WITHDRAWAL,
        account: account,
        appliedAt: df.parse('2019/06/08 06:12:22'))

    List operations = [operation1, operation2, operation3, operation4]

    when(operationRepository.findByAccountAndAppliedAtBetween(any(Account.class), any(), any())).thenReturn(operations)
    when(operationRepository.findSumBeforeDate(anyString())).thenReturn(initialBalance)

    List statement = operationService.findOperationsbyAccountBetweenDates(account, LocalDate.now(), LocalDate.now())
    assertNotNull(statement)
    assertThat(statement.size(), equalTo(3))
    assertThat(statement.first().date, equalTo("07/06/2019"))
    assertThat(statement.first().date, equalTo("07/06/2019"))
    assertThat(statement.first().balance, equalTo("125.00"))
    assertThat(statement.last().operations.size(), equalTo(2))
    assertThat(statement.last().balance, equalTo("175.00"))
  }

  @Test
  void findDebtsBetweenDates() {
    Account account = new Account(id: "shalalaID", accountNo: "777")
    def initialBalance = 0.0G
    Operation operation1 = new Operation(
        amount: 100.0G,
        description: 'Transfer',
        operationType: DEPOSIT,
        account: account,
        appliedAt: df.parse('2019/06/10 01:12:55'))

    Operation operation2 = new Operation(
        amount: 25.0G,
        description: 'Simple Payment',
        operationType: WITHDRAWAL,
        account: account,
        appliedAt: df.parse('2019/06/10 06:12:22'))

    Operation operation3 = new Operation(
        amount: 25.0G,
        description: 'Simple Payment',
        operationType: WITHDRAWAL,
        account: account,
        appliedAt: df.parse('2019/06/07 06:12:22'))

    Operation operation4 = new Operation(
        amount: 25.0G,
        description: 'Simple Payment',
        operationType: WITHDRAWAL,
        account: account,
        appliedAt: df.parse('2019/06/08 06:12:22'))

    List operations = [operation1, operation2, operation3, operation4]

    when(operationRepository.findByAccountAndAppliedAtBetween(any(Account.class), any(), any())).thenReturn(operations)
    when(operationRepository.findSumBeforeDate(anyString())).thenReturn(initialBalance)

    List debts = operationService.findDebts(account, LocalDate.now(), LocalDate.now())

    assertNotNull(debts)
    assertThat(debts.size(), equalTo(2))
    assertThat(debts.first().principal, equalTo("-25.00"))
    assertThat(debts.first().start, equalTo("07/06/2019"))
    assertThat(debts.first().end, equalTo("07/06/2019"))
    assertThat(debts[1].principal, equalTo("-50.00"))
    assertThat(debts[1].start, equalTo("08/06/2019"))
    assertThat(debts[1].end, equalTo("09/06/2019"))

  }
}
