package com.nubank.statement.util


import java.time.LocalDateTime

/**
 * Created by Mario A. Pineda on 2019-06-04.
 */
class DateNuBankUtils {

  public static Date convertToDateViaSqlTimestamp(LocalDateTime dateToConvert) {
    return java.sql.Timestamp.valueOf(dateToConvert)
  }
}
