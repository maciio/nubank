package com.nubank.statement.repository

import com.nubank.statement.entity.Account
import com.nubank.statement.entity.Operation
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

/**
 * Created by Mario A. Pineda on 2019-05-14.
 */
@Repository
interface OperationRepository extends CrudRepository<Operation, String> {

  List<Operation> findByAccountAndAppliedAtBetween(Account account, Date startDate, Date endDate)

  @Query(value = "SELECT SUM( (CASE WHEN OPERATION_TYPE = 0 THEN 1  ELSE -1 END) * AMOUNT ) FROM OPERATION WHERE TRUNCATE(APPLIED_AT) < :date", nativeQuery = true)
  BigDecimal findSumBeforeDate(@Param("date")String date)

}
