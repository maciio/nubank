package com.nubank.statement.repository

import com.nubank.statement.entity.Account
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

/**
 * Created by Mario A. Pineda on 2019-05-14.
 */
@Repository
interface AccountRepository extends CrudRepository<Account, String> {
  List<Account> findByAccountNo(String accountNo)
}
