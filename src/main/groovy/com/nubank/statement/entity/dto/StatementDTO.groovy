package com.nubank.statement.entity.dto

import groovy.transform.ToString

/**
 * Class to show the statement based in operations
 */
@ToString
class StatementDTO {
  String date
  List<OperationDTO> operations
  String balance

  def setBalance(BigDecimal balance) {
    this.balance = balance.trunc(2).toString()
  }

}
