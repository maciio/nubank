package com.nubank.statement.entity.dto

import groovy.transform.ToString

/**
 * Class to show the statement based in operations
 */
@ToString
class OperationDTO {
  String description
  String amount

  def setAmount(BigDecimal amount) {
    this.amount = amount.trunc(2).toString()
  }
}
