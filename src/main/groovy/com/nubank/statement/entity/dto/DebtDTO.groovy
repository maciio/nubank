package com.nubank.statement.entity.dto

import groovy.transform.ToString

/**
 * Created by Mario A. Pineda on 2019-06-03.
 */
@ToString
class DebtDTO {
  String principal
  String start
  String end

  def setPrincipal(BigDecimal principal) {
    this.principal = principal.trunc(2).toString()
  }
}
