package com.nubank.statement.entity

/**
 * Created by Mario A. Pineda on 2019-05-11.
 */

enum OperationType {
  DEPOSIT(1, "Deposit"),
  WITHDRAWAL(2, "Withdrawal"),
  PURCHASE(3, "Purchase")

  final int id
  final String description

  public OperationType(int id, String description) {
    this.id = id
    this.description = description
  }

  int value() { return value }

  String description() { description }

  static OperationType byId(int id) {
    values().find { it.id == id }
  }

  String getDescription() {
    description
  }

}
