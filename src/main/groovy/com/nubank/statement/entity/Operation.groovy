package com.nubank.statement.entity

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.GenericGenerator

import javax.persistence.*
import javax.validation.constraints.NotNull

/**
 * Created by Mario A. Pineda on 2019-05-11.
 */
@Entity
class Operation {

  transient String accountNo

  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  String id

  @ManyToOne
  @JoinColumn
  Account account

  @NotNull
  String description

  @NotNull
  OperationType operationType

  @NotNull
  BigDecimal amount

  @Temporal(TemporalType.TIMESTAMP)
  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss")
  Date appliedAt

  @Temporal(TemporalType.TIMESTAMP)
  @JsonIgnore
  Date createdAt = new Date()

}
