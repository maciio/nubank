package com.nubank.statement.entity

import org.hibernate.annotations.GenericGenerator

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.validation.constraints.NotNull

/**
 * Created by Mario A. Pineda on 2019-05-11.
 */
@Entity
class Account {
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  String id

  @NotNull
  @Column(unique = true)
  String accountNo

  @Column
  BigDecimal balance

  @OneToMany(mappedBy = "account", cascade = [CascadeType.ALL])
  List<Operation> operations

  @Temporal(TemporalType.TIMESTAMP)
  Date createdAt

  @Temporal(TemporalType.TIMESTAMP)
  Date updateAt = new Date()
}
