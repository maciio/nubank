package com.nubank.statement.config


import com.nubank.statement.entity.Operation
import com.nubank.statement.repository.AccountRepository
import com.nubank.statement.repository.OperationRepository
import com.nubank.statement.service.OperationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.annotation.EnableTransactionManagement

import javax.transaction.Transactional
import java.text.SimpleDateFormat

import static com.nubank.statement.entity.OperationType.*

/**
 * Created by Mario A. Pineda on 5/9/16.
 */
@Configuration
@EnableTransactionManagement
@Transactional
class Bootstrap implements CommandLineRunner {

  @Autowired
  AccountRepository accountRepository

  @Autowired
  OperationRepository operationRepository

  @Autowired
  OperationService operationService

  @Override
  void run(String... args) throws Exception {

    Operation operation1 = new Operation(
        amount: 100.0G,
        description: 'Transfer wise',
        operationType: DEPOSIT,
        accountNo: "666",
        appliedAt: new SimpleDateFormat('yyyy/MM/dd HH:mm:ss').parse('2019/05/25 01:12:55'))

    Operation operation2 = new Operation(
        amount: 25.0G,
        description: 'Payment Receipt',
        operationType: DEPOSIT,
        accountNo: "666",
        appliedAt: new SimpleDateFormat('yyyy/MM/dd HH:mm:ss').parse('2019/05/25 06:12:22'))

    Operation operation3 = new Operation(
        amount: 115.0G,
        description: 'Transfer wise',
        operationType: PURCHASE,
        accountNo: "666",
        appliedAt: new SimpleDateFormat('yyyy/MM/dd HH:mm:ss').parse('2019/05/25 11:12:22' ))

    Operation operation4 = new Operation(
        amount: 11.0G,
        description: 'Transfer wise',
        operationType: WITHDRAWAL,
        accountNo: "666",
        appliedAt: new SimpleDateFormat('yyyy/MM/dd').parse('2019/05/26'))

    Operation operation5 = new Operation(
        amount: 1.0G,
        description: 'Transfer wise',
        operationType: DEPOSIT,
        accountNo: "666",
        appliedAt: new SimpleDateFormat('yyyy/MM/dd HH:mm:ss').parse('2019/05/27 02:12:55'))

    Operation operation6 = new Operation(
        amount: 25.0G,
        description: 'Transfer wise',
        operationType: PURCHASE,
        accountNo: "666",
        appliedAt: new SimpleDateFormat('yyyy/MM/dd HH:mm:ss').parse('2019/05/28 22:12:55'))

    [operation1, operation2, operation4, operation3, operation6, operation5].each { op ->
      operationService.saveOperation(op)
    }



  }
}
