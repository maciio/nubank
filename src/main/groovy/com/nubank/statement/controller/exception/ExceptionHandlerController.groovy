package com.nubank.statement.controller.exception

import com.fasterxml.jackson.databind.exc.InvalidFormatException
import groovy.util.logging.Slf4j
import org.springframework.http.HttpStatus
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.*

/**
 * Simple class that doesn;t allow to the user decypher catch the errors just in case happen
 * Security Catch
 * Created by Mario A. Pineda on 5/21/18.
 */
@RestController
@ControllerAdvice
@Slf4j
public class ExceptionHandlerController {

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  Object handleIllegalArgument(final Throwable ex) {
    [code: "BPE", message: "Message Received"] // Bind properties Exception
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(InvalidFormatException.class)
  Object handleInvalidFormatException(final Throwable ex) {
    [code: "IFE", message: "Message Received"] // Invalid Format Exception
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(InvalidDataException.class)
  Object handleInvalidDataException(final Throwable ex) {
    [code: "IDA", message: "Message Received"] // Invalid Data Exception
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(Exception.class)
  Object handleInvalidException(final Throwable ex) {
    [code: "SOO", message: "Message Received"] // 500 Server
  }

}
