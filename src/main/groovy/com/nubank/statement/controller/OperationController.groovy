package com.nubank.statement.controller

import com.nubank.statement.controller.exception.InvalidDataException
import com.nubank.statement.entity.Operation
import com.nubank.statement.service.AccountService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

import java.time.LocalDate

import static org.springframework.http.HttpStatus.OK

/**
 * Basic controller for operations
 * Created by Mario A. Pineda on 2019-05-11.
 */
@Slf4j
@RestController
@RequestMapping(value = "/account", produces = "application/json")
@ResponseStatus(OK)
class OperationController {

  @Autowired
  AccountService accountService

  @RequestMapping(value = "/operation", method = [RequestMethod.POST])
  def addOperation(@Validated @RequestBody Operation operation) {
    accountService.addOperation(operation)
    [code: "OAS", message: "Message Received"] //TODO Operation Account Succeed

  }

  @RequestMapping(value = "/{accountNo}/balance", method = RequestMethod.GET)
  def getAccountBalance(@PathVariable("accountNo") String accountNo) {
    validFormat(accountNo)
    BigDecimal balance = accountService.getBalance(accountNo)
    [balance: balance.trunc(2).toString()]
  }

  @RequestMapping(value = "/{accountNo}/statement", method = RequestMethod.GET)
  def getAccountStatement(@PathVariable("accountNo") String accountNo,
                          @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                          @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
    log.debug("params[startsDate: $startDate, endDate: $endDate]")
    validFormat(accountNo)
    accountService.getStatement(accountNo, startDate, endDate)
  }

  @RequestMapping(value = "/{accountNo}/debt-periods", method = RequestMethod.GET)
  def getAccountDebts(@PathVariable("accountNo") String accountNo,
                      @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                      @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
    log.debug("params[startsDate: $startDate, endDate: $endDate]")
    validFormat(accountNo)
    accountService.getPeriodsOfDebts(accountNo, startDate, endDate)
  }

  private void validFormat(String accountNo) {
    if (!accountNo?.isNumber()) {
      throw new InvalidDataException()
    }
  }

}
