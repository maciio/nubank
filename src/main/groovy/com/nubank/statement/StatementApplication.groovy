package com.nubank.statement

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext

import javax.annotation.PostConstruct

@SpringBootApplication(scanBasePackages = "com.nubank.statement")
class StatementApplication {

  static void main(String... args) {
    ApplicationContext ctx = SpringApplication.run(StatementApplication, args)
  }

  @PostConstruct
  void started() {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
  }
}
