package com.nubank.statement.service

import com.nubank.statement.entity.Account
import com.nubank.statement.entity.Operation
import com.nubank.statement.entity.dto.DebtDTO
import com.nubank.statement.entity.dto.OperationDTO
import com.nubank.statement.entity.dto.StatementDTO
import com.nubank.statement.repository.AccountRepository
import com.nubank.statement.repository.OperationRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate

import static com.nubank.statement.entity.OperationType.DEPOSIT
import static com.nubank.statement.util.DateNuBankUtils.convertToDateViaSqlTimestamp
import static java.math.BigDecimal.ZERO
import static java.time.LocalTime.MAX
import static java.time.LocalTime.MIN

/**
 * Created by Mario A. Pineda on 2019-05-12.
 */
@Slf4j
@Service
class OperationService {

  @Autowired
  AccountRepository accountRepository

  @Autowired
  OperationRepository operationRepository

  private static final DateFormat df = new SimpleDateFormat('dd/MM/yyyy')

  def saveOperation(Operation operation) {
    log.debug(operation.dump())
    List<Account> accountList = accountRepository.findByAccountNo(operation.accountNo)
    Account account
    if (accountList) {
      log.info "Account found!, updating current balance"
      account = accountList.first()
      account.balance = updateAccountBalance(account, operation)
      accountRepository.save(account)
    } else {
      log.info "New Account to be created"
      operation.amount = (operation.operationType == DEPOSIT) ? operation.amount : operation.amount.negate()
      account = accountRepository.save(new Account(
          accountNo: operation.accountNo,
          balance: operation.amount,
          createdAt: new Date()))
    }
    operation.account = account
    operationRepository.save(operation)
    account
  }

  List<StatementDTO> findOperationsbyAccountBetweenDates(Account account, LocalDate startDate, LocalDate endDate) {
    List<Operation> operations = operationRepository.findByAccountAndAppliedAtBetween(account,
        convertToDateViaSqlTimestamp(startDate.atTime(MIN)), convertToDateViaSqlTimestamp(endDate.atTime(MAX)))

    BigDecimal balanceOneDayBefore = operationRepository.findSumBeforeDate(startDate.toString()) ?: ZERO

    log.debug("Checking the balanceOneDayBefore: $balanceOneDayBefore")
    Map<String, List<Operation>> opMap = operations.sort { it.appliedAt }.groupBy(formatAppliedAt)
    List<StatementDTO> statement = []
    StatementDTO dto
    for (e in opMap) {
      dto = new StatementDTO()
      dto.date = e.key
      dto.operations = e.value.collect { op ->
        new OperationDTO(description: "${op.operationType.description} - ${op.description}",
            amount: op.amount)
      }
      e.value = e.value.collect(negateAmountIfApply)
      balanceOneDayBefore = (balanceOneDayBefore + e.value*.amount.sum())
      dto.balance = balanceOneDayBefore
      statement << dto
    }
    statement
  }

  List findDebts(Account account, LocalDate startDate, LocalDate endDate) {
    List<Operation> operations = operationRepository.findByAccountAndAppliedAtBetween(account,
        convertToDateViaSqlTimestamp(startDate.atTime(MIN)), convertToDateViaSqlTimestamp(endDate.atTime(MAX)))
    BigDecimal balance = operationRepository.findSumBeforeDate(startDate.toString()) ?: ZERO
    log.debug("Checking the initialBalance--> $balance")

    Calendar c1 = Calendar.getInstance()
    List<DebtDTO> debts = []
    Map<String, List<Operation>> opMap = operations.sort { it.appliedAt }.groupBy(formatAppliedAt)
    DebtDTO dto
    for (e in opMap) {
      e.value = e.value.collect(negateAmountIfApply)
      balance = (balance + e.value*.amount.sum())
      if (balance < ZERO) {
        if (!debts.isEmpty()) {
          c1.setTime(df.parse(e.key))
          c1.add(Calendar.DATE, -1)
          debts.last().end = df.format(c1.getTime())
        }
        dto = new DebtDTO(principal: balance, start: e.key, end: e.key)
        debts << dto
      } else {
        if (!debts.isEmpty()) {
          c1.setTime(df.parse(e.key))
          c1.add(Calendar.DATE, -1)
          debts.last().end = df.format(c1.getTime())
        }
      }

    }
    debts
  }

  private BigDecimal updateAccountBalance(Account account, Operation operation) {
    if (operation.operationType == DEPOSIT) {
      return account.balance += operation.amount
    }
    account.balance -= operation.amount
  }

//common closures
  private def formatAppliedAt = { op -> df.format(op.appliedAt) }

  private def negateAmountIfApply = { op ->
    if (op.operationType != DEPOSIT) {
      op.amount = op.amount.negate()
    }
    op
  }
}
