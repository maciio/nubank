package com.nubank.statement.service

import com.nubank.statement.entity.Account
import com.nubank.statement.entity.Operation
import com.nubank.statement.entity.dto.DebtDTO
import com.nubank.statement.entity.dto.StatementDTO
import com.nubank.statement.repository.AccountRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.time.LocalDate

/**
 * Created by Mario A. Pineda on 2019-05-14.
 */
@Slf4j
@Service
class AccountService {

  @Autowired
  AccountRepository accountRepository

  @Autowired
  OperationService operationService

  Account addOperation(Operation operation) {
    operationService.saveOperation(operation)
  }

  BigDecimal getBalance(String accountNo) {
    List<Account> accounts = accountRepository.findByAccountNo(accountNo)
    if (accounts) {
      Account account = accounts.first()
      return account.balance
    }
    0.00G
  }

  List<StatementDTO> getStatement(String accountNo, LocalDate startDate, LocalDate endDate) {
    List<Account> accounts = accountRepository.findByAccountNo(accountNo)
    if (accounts) {
      Account account = accounts.first()
      return operationService.findOperationsbyAccountBetweenDates(account, startDate, endDate)
    }
    []
  }

  List<DebtDTO> getPeriodsOfDebts(String accountNo, LocalDate startDate, LocalDate endDate) {
    List<Account> accounts = accountRepository.findByAccountNo(accountNo)
    if (accounts) {
      Account account = accounts.first()
      return operationService.findDebts(account, startDate, endDate)
    }
    []
  }
}
